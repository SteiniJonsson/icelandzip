# README #

### What is this repository for? ###

Upphaflegt markmið þessa verkefnis var að fá GPS hnit á póstnúmerum.
Þar sem engin opinber skráning er til þá tók ég staðfangaskrá og reiknaði meðal GPS hnit hvers póstnúmers og skrifaði í póstnúmeraskrá.

Haldið er utan um staðsetningu fasteigna í staðfangaskrá.

Staðföng eru til þess gerð að hjálpa hverjum þeim sem gagn hefur af að afla upplýsinga um staðsetningu fólks, 
landeigna, mannvirkja, svæða eða annars sem þörf er á. 
Staðföng nýtast þannig öllum almenningi með beinum hætti eða í gegnum hinar ýmsu stofnanir og fyrirtæki, 
svo sem stjórnvöld, neyðarþjónustu, rannsóknaraðila, veitufyrirtæki og fleiri.

### How do I get set up? ###

Staðfangaskrá er hægt að sækja á slóðina https://opingogn.is/dataset/stadfangaskra
Í solution eru 2 project.
ZipDB inniheldur SQL script til að setja upp test database og töflur.
ZipETL inniheldur SSIS pakka sem les inn staðfangaskrána úr flat file (STADFANG.dsv) og reiknar

### Who do I talk to? ###

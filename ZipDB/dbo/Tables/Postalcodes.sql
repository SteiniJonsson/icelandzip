﻿create table Postalcodes
(
  Postcode    char ( 3 )        not null,
  Municipal   varchar ( 50 )    not null,
  ServiceType varchar ( 50 )    not null,
  Area        varchar ( 50 )    not null,
  [Location]  varchar ( 50 )    not null,
  Lat_WGS84   decimal ( 20, 9 ) not null, 
  Long_WGS84  decimal ( 20, 9 ) not null, 
  GPSPoint    geography         not null
 constraint PK_Postcodes primary key clustered ( Postcode )
)

﻿create table PostalcodeDemoData
(
  NoOfItems int        not null,
  Amount    int        not null,
  Postcode  char ( 3 ) not null
)
/*
insert PostalcodeDemoData ( NoOfItems, Amount, Postcode )
select ( abs ( checksum ( NewId () ) ) % 100 )   + 1 [NoOfItems]
     , ( abs ( checksum ( NewId () ) ) % 10000 ) + 1 [Amount]
     , Postcode                                   [Postcode]
from   Postalcodes
*/

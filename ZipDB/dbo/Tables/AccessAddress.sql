﻿create table AccessAddress
(
  AccessAddressPK   int               not null identity ( 1, 1 ),
  Hnitnum           char    (   8 )   not null, -- 8   10095286  
  Svfnr             char    (   4 )   not null, -- 4   0000  
  Byggd             char    (   2 )   not null, -- 2   01  
  Landnr            char    (   6 )   not null, -- 6   111632  
  Heinum            char    (   7 )   not null, -- 7   1000751  
  Fasteignaheiti    varchar (  50 )   not null, -- 33   2.Gata v/Rauðavatn 29    
  Matsnr            int               not null, -- 7  
  Postnr            char    (   3 )   not null, -- 3   113  
  Heiti_nf          varchar (  50 )   not null, -- 40   2.Gata v/Rauðavatn  
  Heiti_tgf         varchar (  50 )   not null, -- 42   2.Gata v/Rauðavatn  
  Husnr             int               not null, -- 4   29        
  Bokst             varchar (   3 )   not null, -- 3  
  Vidsk             varchar (  50 )   not null, -- 25  
  Serheiti          varchar (  50 )   not null, -- 39  
  Dags_inn          date              not null, -- 10   10.11.2008  
  Dags_leidr        date              not null, -- 10   10.02.2015  
  Gagna_eign        varchar (  50 )   not null, -- 16   Þjóðskrá Íslands  
  Teghnit           int               not null, -- 1   1  
  Yfirfarid         int               not null, -- 1   2  
  Athugasemd        varchar ( 255 )   not null, -- 212 póstnr. uppfært skv. þekju Íslandspósts
  Nakv_XY           decimal ( 10, 1 ) not null, -- 5  
  X_ISN93           decimal ( 20, 9 ) not null, -- 25   365037,809090909  
  Y_ISN93           decimal ( 20, 9 ) not null, -- 24   404228,736363636  
  Lat_WGS84         decimal ( 20, 9 ) not null, -- 16   64,11438660967  
  Long_WGS84        decimal ( 20, 9 ) not null, -- 17   -21,7702940057837  
  Dags_utgafa       date              not null, -- 10  21.05.2017
  GPSPoint          as geography::Point ( Lat_WGS84, Long_WGS84, 4326 ) 
)

﻿----------------------------------------------------------------------------------------------------
-- Create database template with primary filegroup for system objects and separated filegroups
-- for data and indexes.
----------------------------------------------------------------------------------------------------
use [master]
create database [ZipDB]
containment = none
on primary 
( 
  name = N'ZipDB_Primary', 
  filename = N'E:\SQLData\Default\ZipDB_Primary.mdf' , 
  size = 50 MB,
  maxsize = 1 GB, 
  filegrowth = 50 MB
)
log on 
( 
  name = N'ZipDB_Log', 
  filename = N'E:\SQLTLogs\Default\ZipDB_Log.ldf' , 
  size = 100 MB, 
  maxsize = 5 GB, 
  filegrowth = 100 MB 
)
go

alter database [ZipDB] set compatibility_level = 140
alter database [ZipDB] set recovery simple 
go

use [ZipDB]
go

alter database [ZipDB] add filegroup [Data1] 
go

alter database [ZipDB]
add file
( 
  name = N'ZipDB_Data1', 
  filename = N'E:\SQLData\Default\ZipDB_Data1.ndf' , 
  size = 100 MB, 
  maxsize = 5 GB, 
  filegrowth = 100 MB
)
to filegroup [Data1]

if not exists (select name from sys.filegroups where is_default=1 and name = N'Data1') 
alter database [ZipDB] modify filegroup [Data1] default
go

alter database [ZipDB] add filegroup [Index1] 
go

exec sp_changedbowner @loginame = N'sa'
go

use [master]
